import { writable, get } from 'svelte/store';

const UPDATE_GRANULARITY = 100;
const UPDATE_INTERVAL = 1000 / UPDATE_GRANULARITY;
const DECREMENT_PER_UPDATE = 1 / UPDATE_GRANULARITY;

export function createTimedOperation(duration, action) {

    if (!action) {
        console.error("No action specified for timed operation");
    }

    let initialValue;

    const unsubscribe = duration.subscribe(value => { initialValue = value });

    const { subscribe, update } = writable(initialValue);

    return {
        subscribe,
        initialValue: () => {
            return initialValue;
        },
        start: (set) => {
            update(n => initialValue);
            const timer = setInterval(() => {
                update(n => {
                    if (n <= 0) {
                        clearInterval(timer);
                        new Promise(action);
                        return initialValue;
                    }
                    return n - DECREMENT_PER_UPDATE
                });
            }, UPDATE_INTERVAL);
        }
    }
}