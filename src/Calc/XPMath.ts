import { writable, derived, readable } from 'svelte/store';

export function nextLevelXP(playerLevel) {

    return derived(playerLevel, ($playerLevel: number) => {
        let nextLevel = Math.floor($playerLevel) + 1

        return 25 * nextLevel * nextLevel - 25 * nextLevel;
    });
};

export function levelForXP(playerXP) {

    return derived(playerXP, ($playerXP: number) => Math.floor(25 + Math.sqrt(625 + 100 * $playerXP)) / 50);
};