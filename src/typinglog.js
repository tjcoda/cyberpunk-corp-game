import { writable, derived, readable } from 'svelte/store';

const UPDATES_PER_SECOND = 20;
const UPDATE_INTERVAL = 1000 / UPDATES_PER_SECOND;

export function createTypingLog() {

    const { subscribe, set } = writable('');
    const inputLog = writable('');

    let inputLogProxy;

    inputLog.subscribe(value => {
        inputLogProxy = value;
    })

    let outputLogProxy;

    subscribe(value => {
        outputLogProxy = value;
    });

    const timer = setInterval(() => {
        if (inputLogProxy.length > outputLogProxy.length) {
            set(inputLogProxy.substring(0, outputLogProxy.length + 1));
        }
        else if (inputLogProxy.split("\n").length > 8) {
            set("");
            inputLog.set("");
        }
    }, UPDATE_INTERVAL);

    return {
        subscribe,
        logWriter: () => inputLog,
    }
}

