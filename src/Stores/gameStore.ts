import { writable, derived, readable } from 'svelte/store';
import { createTimedOperation } from '../Mechanics/timedoperation';
import { createTypingLog } from '../typinglog';

import { playerLevel, playerXP } from './playerStore';

export const inventoryVisible = writable(false);

export const inventory = writable({});

export const log = createTypingLog();

export const timeAHackTakes = derived(playerLevel, $playerLevel => $playerLevel * 2);
export const timeAScavengeTakes = readable(1, () => { });

export const hackTimer = createTimedOperation(timeAHackTakes, () => {
    playerXP.update(xp => xp + 10);
    log.logWriter().update(l => l + `Hacked...\n`);
});

const scavengeProbabilities = {
    badger: 0.8,
    techscrap: 0.5,
    crisppacket: 0.1,
    sodacan: 0.2,
};

function incrementResource(key, quantity) {

    if (quantity === undefined) {
        quantity = 1;
    }

    inventory.update(inv => {
        let newInv = {
            ...inv,
            [key]: (inv[key] || 0) + quantity
        };

        return newInv;
    });
}

export const scavengeTimer = createTimedOperation(timeAScavengeTakes, () => {
    for (var item in scavengeProbabilities) {
        let roll = Math.random();
        var probability = scavengeProbabilities[item];
        if (roll >= probability) {
            incrementResource(item, undefined);
        }
    }

    log.logWriter().update(l => l + `Scavenged...\n`);
});

