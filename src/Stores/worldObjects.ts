export const resourceProperties = {
    badger: { nicename: "Badger" },
    techscrap: { nicename: "Tech Scrap" },
    crisppacket: { nicename: "Crisp Packet" },
    sodacan: { nicename: "Soda Can" },
}