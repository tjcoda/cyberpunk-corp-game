import { writable, derived, readable } from 'svelte/store';
import * as XPMath from '../Calc/XPMath.js'

export const playerLocation = writable('realworld');
export const playerXP = writable(0);
export const playerLevel = XPMath.levelForXP(playerXP);

export const nextLevelXP = XPMath.nextLevelXP(playerLevel);